//esta es la clase que se encarga de calcular los impuestos, las utilidades, y el concepto de credito

public class Impuestos{
  
  double utilidades,conceptoCredito,impuesto;
    
  public Impuestos(){
  }//metodo constructor sin parametros
  
  public void setUtilidades(double ingresos, double gastos){
    
    this.utilidades = ingresos-gastos;
  }
  
  public double getUtilidades(){
    return utilidades;
  }
  
  public void setConceptoCredito(int cantidadHijos, boolean conyuge){
   
    double montoCreditoTotal=0;
    
    if (cantidadHijos == 0){
      montoCreditoTotal+=0;
    } else {
      montoCreditoTotal+=cantidadHijos*17760;
    }
    
    if (conyuge != true){
      montoCreditoTotal+=0;
    } else {
      montoCreditoTotal+=26760;
    }
    
    this.conceptoCredito=montoCreditoTotal;
    
  }//fin metodo setConceptoCredito
  
  public double getConceptoCretido(){
    return conceptoCredito;
  }
  
  public void setImpuesto(double utilidades){
    this.utilidades=utilidades;
    
    if (utilidades<=3496000){
      impuesto=0;
    } else {
      if (utilidades<=5220000){
        impuesto=utilidades*0.1;
      } else {
        if (utilidades<=8708000){
          impuesto=utilidades*1.5;
        } else {
          if (utilidades<=17451000){
            impuesto=utilidades*2.0;
          } else {
            impuesto=utilidades*2.5;
          }
        }
      }
    }
  }//fin metodo setImpuesto
  
  public double getImpuesto(){
    return impuesto;
  }
  
  public String toString (){
    return "El impusto aplicado es de: "+getImpuesto(); 
  }
  
}//fin clase 