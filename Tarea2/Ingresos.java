//esta clase es la que se encarga de registrar las facturas de los cobros por servicios profesionalesl, es decir, los ingresos
//registra datos como el numero de factura, fecha, monto de cada una de las facturas y el total de los ingresos registrados

public class Ingresos{
  
  int numeroFactura;
  String fechaFactura;
  double montoFactura,totalIngresos=0;
  
  public Ingresos(){
  }//metodo constructor sin parametros
  
  public void setNumeroFactura(int numeroFactura){
    this.numeroFactura=numeroFactura;
  } 
  
  public int getNumeroFactura(){
    return numeroFactura;
  }
  
  public void setFechaFactura(String fechaFactura){
    this.fechaFactura=fechaFactura;
  }
  
  public String getFechaFactura(){
    return fechaFactura;
  }
  
  public void setMontoFactura(double montoFactura){
    this.montoFactura=montoFactura;
  }
  
  public double getMontoFactura(){
    return montoFactura;
  }
  
  public void setTotalIngresos(double montoFactura){
    totalIngresos+=montoFactura;
  }
  
  public double getTotalIngresos(){
    return totalIngresos;
  }
  
  public String toString(){
    return "La informacion de las facturas es la siguiente "+getNumeroFactura()+ ", "+getFechaFactura()+ ", "+getMontoFactura()+
          "\n y el total de los ingresos es de: "+getTotalIngresos();
  }

}//fin clase