//esta es la clase que tendra interaccion con la persona usuaria para que esta registre las facturas de cobro y pago

import javax.swing.JOptionPane;

public class Principal{
  
  public static void main (String arg[]){
    
    Ingresos ingresos = new Ingresos();
    Gastos gastos = new Gastos();
    Impuestos impuestos = new Impuestos();
    
    int opcion;
    String valorLeido;
    
   JOptionPane.showMessageDialog(null,"Bienvenidx, en este programa podras calcular el "+
                                    "\n impuesto de la renta a pagar anualmente al Estado"); 
   
    opcion=1;
    while(opcion!=6){
        valorLeido=JOptionPane.showInputDialog("*****Menu*****"
                                           +"\n1.  Ingresar las facturas por servicios profesionales."
                                           +"\n2.  Ingresar gastos."
                                           +"\n3.  Listado de ingresos"
                                           +"\n4.  Listado de gastos"
                                           +"\n5.  C�lculo del impuesto de renta."
                                           +"\n6.  Salir.");
        
        opcion=Integer.parseInt(valorLeido);
        
  switch(opcion){
    
    case 1:
      int numeroIngreso;
      String fechaIngreso;
      double montoIngreso;
      
      valorLeido=JOptionPane.showInputDialog("Ingrese el numero de la factura a ingresar");
      numeroIngreso=Integer.parseInt(valorLeido);
      ingresos.setNumeroFactura(numeroIngreso);
      
      valorLeido=JOptionPane.showInputDialog("Ingrese la fecha de la factura a ingresar");
      fechaIngreso=valorLeido;
      ingresos.setFechaFactura(fechaIngreso);
      
      valorLeido=JOptionPane.showInputDialog("Ingrese el monto de la factura a ingresar");
      montoIngreso=Double.parseDouble(valorLeido);
      ingresos.setMontoFactura(montoIngreso);
      ingresos.setTotalIngresos(montoIngreso);
      
     break;
    
    case 2:
      
      int numeroGasto;
      String fechaGasto;
      double montoGasto;
      
      valorLeido=JOptionPane.showInputDialog("Ingrese el numero de la factura de gasto a ingresar");
      numeroGasto=Integer.parseInt(valorLeido);
      gastos.setNumeroFactura(numeroGasto);
      
      valorLeido=JOptionPane.showInputDialog("Ingrese la fecha de la factura de gastos a ingresar");
      fechaGasto=valorLeido;
      gastos.setFechaFactura(fechaGasto);
      
      valorLeido=JOptionPane.showInputDialog("Ingrese el monto de la factura de gastos a ingresar");
      montoGasto=Double.parseDouble(valorLeido);
      gastos.setMontoFactura(montoGasto);
      gastos.setTotalGastos(montoGasto);
     
      break;
    
    case 3:
      
       JOptionPane.showMessageDialog(null,"El total de ingresos es de: "+ingresos.getTotalIngresos());
      
      break;
      
    case 4:
      
      JOptionPane.showMessageDialog(null,"El total de gastos es de: "+gastos.getTotalGastos());
      
      break;
      
    case 5:
       
       int cantidadHijos;
       boolean conyuge;
       double totalImpuestoAnual;
       
       valorLeido=JOptionPane.showInputDialog("Tiene hijos?, si no digite 0, si si, digite el numero de hijos");
       cantidadHijos=Integer.parseInt(valorLeido);
       
       valorLeido=JOptionPane.showInputDialog("Tiene esposa o esposo?");
       
       if (valorLeido.equalsIgnoreCase("Si")){
         conyuge = true;
       } else {
         conyuge = false;
       }
       
       impuestos.setConceptoCredito(cantidadHijos,conyuge);
       impuestos.setUtilidades(ingresos.getTotalIngresos(),gastos.getTotalGastos());
       impuestos.setImpuesto(impuestos.getUtilidades());
       double calculoImpuestoTotal = impuestos.getImpuesto()-impuestos.getConceptoCretido();
         
       JOptionPane.showMessageDialog(null," el monto total de ingresos "+ingresos.getTotalIngresos()+" ,monto total de gastos "+gastos.getTotalGastos()+
                                     "\n las utilidades "+impuestos.getUtilidades()+" credito aplicado "+impuestos.getConceptoCretido()+
                                     "\n el monto a pagar es "+impuestos.getImpuesto()+" aplicando el credito, lo que debe pagar es "+calculoImpuestoTotal);

            
            
      break;
      
    case 6:
     
      System.exit(0);
      
      break;
    
    default:JOptionPane.showMessageDialog(null,"Esa opcion no existe ");//en caso de seleccionar una opcion que no disponible
    
  }//fin switch
  
    }//fin while
  }//fin main
  
}//fin clase