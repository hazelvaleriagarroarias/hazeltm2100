//esta clase es la que se encarga de registrar los gastos de la persona, en ella se guardara y retornara lo siguiente:
//numero de factura, fecha de cada factura, el monto de cada una y el total de los gastos registrados

public class Gastos{
  
  int numeroFactura;
  String fechaFactura;
  double montoFactura,totalGastos=0;
  
  public Gastos(){
  }//metodo constructor sin parametros
  
  public void setNumeroFactura(int numeroFactura){
    this.numeroFactura=numeroFactura;
  }
  
  public int getNumeroFactura(){
    return numeroFactura;
  }
  
  public void setFechaFactura(String fechaFactura){
    this.fechaFactura=fechaFactura;
  }
  
  public String getFechaFactura(){
    return fechaFactura;
  }
  
  public void setMontoFactura(double montoFactura){
    this.montoFactura=montoFactura;
  }
  
  public double getMontoFactura(){
    return montoFactura;
  }
  
   public void setTotalGastos(double montoFactura){
    totalGastos+=montoFactura;
  }
  
  public double getTotalGastos(){
    return totalGastos;
  }
  
  public String toString(){
    return "La informacion de las facturas de los gastos es la siguiente "+getNumeroFactura()+ ", "+getFechaFactura()+ ", "+getMontoFactura()+
           "\n y el total de los costos de gastos es de: "+getTotalGastos();
  }

}//fin clase