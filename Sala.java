public class Sala {

  private int capacidad,asientosReservados;
  
  public Sala (){
    capacidad=30;
  }
  
  public Sala (int capacidad){
  this.capacidad=capacidad;
  }

  public void setCapacidad (int capacidad){
  this.capacidad=capacidad;
  }
  
  public int getCapacidad(){
    return capacidad;
  }
  
  public int getAsientosReservados (){
  return asientosReservados;
  }
  
  public void ocuparAsiento(int asientosVendidos){
    asientosReservados+=asientosVendidos;
  }
  
  public void desocuparAsiento(int asientosDevueltos){
    asientosReservados-=asientosDevueltos;
  }
  
  public int getEspacioDisponible(){
    return capacidad-asientosReservados;
  }
    
}// fin de clase