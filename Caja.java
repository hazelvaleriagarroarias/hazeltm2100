public class Caja {

  private long recaudado;
  
  public Caja (){
    recaudado=0; 
  }
  
  public Caja(long monto){
    recaudado=monto;
  }
  
  public void setRecaudado(long monto){
    recaudado=monto;
  }
  
  public long getRecaudado(){
    return recaudado;
  }
  
  public void aumentarRecaudado(long monto){
    recaudado+=monto;
  }
  
  public void disminuirRecaudado(long monto){
    recaudado-=monto;
  }
  
}//fin de clase